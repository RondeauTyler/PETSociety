package pet.service;

import org.springframework.stereotype.Service;

import pet.dao.UserDao;
import pet.model.User;

@Service
public class UserServiceImpl implements UserService{
	
	UserDao dao;
	
	public UserServiceImpl(UserDao dao) {
		this.dao=dao;
	}
	
	

	@Override
	public void register(User user) {
		dao.save(user);
	}
}

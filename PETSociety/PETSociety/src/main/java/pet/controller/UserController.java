package pet.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pet.model.User;
import pet.service.UserService;

@RestController
public class UserController {

	private UserService serv;
	
	public UserController() {
		// TODO Auto-generated constructor stub
	}
	
	@Autowired
	public UserController(UserService serv) {
		this.serv = serv;
	}
	
	@PostMapping(value="/registerUser")
	@ResponseStatus(value = HttpStatus.CREATED)
	public void register(HttpServletRequest req) {
		String username = req.getParameter("registerUsername");
		String password = req.getParameter("registerPassword");
		String email = req.getParameter("registerEmail");
		System.out.println(username + ", " + password);
		
		User newUser = new User(username,password,email);
		
	
		serv.register(newUser);
	}
	
	@GetMapping(value="/getUser")
	public User getUser() {
		User user = new User();
		
		return user;
	}
}

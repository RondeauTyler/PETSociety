package pet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pet.model.Profile;

@Repository
public interface ProfileDao extends JpaRepository<Profile, Integer>{
	
}

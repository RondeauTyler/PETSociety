package pet.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="pet_user")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int userId;
	
	@Column(unique=true)
	private String petUsername;
	
//	@Column(name="pet_password")
	private String petPassword;
	
	@Column(unique=true)
	private String petEmail;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "profile_id_fk"/* , referencedColumnName="profile_id" */)
	private Profile myProfile;
	
	public User(String username, String password, String email) {
		this.petUsername=username;
		this.petPassword=password;
		this.petEmail=email;
	}
	
}

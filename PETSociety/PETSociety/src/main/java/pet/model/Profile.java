package pet.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Entity
@Table(name="profile")
public class Profile {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name="profile_id")
	private int profileId;
	
//	@Column(name="first_name")
	private String firstName;
	
//	@Column(name="last_name")
	private String lastName;
	
//	@Column(name="description")
	private String description;
	
	private String profilePic;
	
	@JsonIgnore
	@OneToMany(mappedBy = "myProfile", fetch=FetchType.LAZY)
	private List<Post> posts = new ArrayList<>();
	
	@OneToOne(mappedBy= "myProfile")
	private User myUser;
	
}
